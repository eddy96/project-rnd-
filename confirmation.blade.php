<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>
	@extends('layouts.adminbar')
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/admin1.css">

</head>
<body>
@section('admin')
	<div class="bodi">
		<div class="navbar">
			<div class="sidebar">
				<div class="container">
					<div class="container-kecil">
						<div class="logo">
							<div class="title">
								<h1>BTE</h1>
								<p>Admin Panel</p>
							</div>
						</div>
					
					<div class="home1">
						<div class="home active" id="js-menu">
			
						<div class="nav" id="js-menu">
							<div class="navnav">
								<div class="nav-choice nav-active">
								<a href="admin1.html"><img src="assets/AdminPanel/Icon/PaymentConfirmBtn.png">
								</a>
								</div>

								<div class="nav-choice">
								<a href="admin2.html"><img src="assets/AdminPanel/Icon/EditDataBtn.png">
								</a>
								</div>
								<div class="nav-choice ex">
								<a href="#Logout"><img src="assets/AdminPanel/Icon/AdminLogoutBtn.png">
								</a>
								</div>
							</div>
						</div>
						
						
						</div>
						<div class="blur active1" id="js-menu1">
							
						</div>
					</div>
					
					</div>
					
				</div>
			</div>

			
		</div>
		
		<div class="content">
			<div class="inside-content">
				<div class="title-content">
					<h1>Payment Confirmation</h1>
				</div>
				<div class="body-content">
						@foreach($users as $user)
                    	@if($user->id != 1)
						<div class="data">
							<h2>{{$user->group}}</h2>
							<hr>
							<div>
								<div><p>File: </p></div>
								<div>
									<button type="submit" id="{{$user->group}}Btn"><a href="{{ Storage::disk('local')->url($user->payment->receipt ?? 'CV Not Found')}}">View</a></button>
									<div id="{{$user->group}}Modal" class="modal">

  										<!-- Modal content -->
  										<div class="modal-content">
  								  		<span class="close">&times;</span>
  							 	 		<p>Some text in the Modal..</p>
  							 	 		<img src="">
  									</div>

							</div>
							<form method="POST" action="/admin" enctype="multipart/form-data">
								@csrf
								
								<input type="submit" name="paid" value="Verify">
								<input type="hidden" name="id" value="{{$user->id}}">
						   </form>
								</div>
								<p>Status:</p>
							</div>
						</div>
						
			</div>
			
		</div>
	</div>
	
<script type="text/javascript" src="js/admin1.js"></script>



	
</body>
</html>