@extends('layouts.adminbar')
<link rel="stylesheet" type="text/css" href="css/admin1.css">
@section('admin')
<div class="content">
        <div class="inside-content">
            <div class="title-content">
                <h1>Payment Confirmation</h1>
            </div>
            <div class="body-content">
                <!-- <div class="data-row">

                    </div> -->
                    @foreach($users as $user)
                    @if($user->id != 1)
                    <div class="data">
                        <h2>{{$user->group}}</h2>
                        <hr>
                        <div>
                            <div><p>File: </p></div>
                            <div>

                            <button type="submit" id="{{$user->group}}Btn"><a href="{{ Storage::disk('local')->url($user->payment->receipt ?? 'CV Not Found')}}">View</button>

                                <div id="{{$user->group}}Modal" class="modal">

                                      <!-- Modal content -->
                                      <div class="modal-content">
                                        <span class="close">&times;</span>
                                        <p>Some text in the Modal..</p>
                                        <img src="">
                                  </div>

                        </div>
                        <form method="POST" action="/admin" enctype="multipart/form-data">
                            @csrf
                                <input type="submit" name="paid" value="Verify">
                             <input type="hidden" name="id" value="{{$user->id}}">
                                </form>
                            </div>
                            <p>Status:

                            </p>
                        </div>
                    </div>
                    <script type="text/javascript">

                        var modal = document.getElementById('Modal');

                        // Get the button that opens the modal
                        var btn = document.getElementById("myBtn");

                        // Get the <span> element that closes the modal
                        var span = document.getElementsByClassName("close")[0];

                        // When the user clicks on the button, open the modal
                        btn.onclick = function() {
                          modal.style.display = "block";
                        }

                        // When the user clicks on <span> (x), close the modal
                        span.onclick = function() {
                          modal.style.display = "none";
                        }

                        // When the user clicks anywhere outside of the modal, close it
                        window.onclick = function(event) {
                          if (event.target == modal) {
                            modal.style.display = "none";
                          }
                        }


                        </script>
                    @endif
                    @endforeach

        </div>

    </div>
</div>
@endsection
