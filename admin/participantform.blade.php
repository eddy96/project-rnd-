@extends('layouts.adminbar')
<link rel="stylesheet" type="text/css" href="../../css/admin3.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
@section('admin')
<div class="content">

        <div class="inside-content">

            <div class="title-content">
            <h1>{{$users->find($id)->group}}</h1>
            </div>

            @foreach($uploads as $email)
            <form method="POST" action="/admin/{id}/edit" enctype="multipart/form-data">
            <div class="body-content">
                <div class="middle-content">

                <div class="middle-bio">
                    <div class="md-content">
                        <h1>{{$email->role}}</h1>
                        <div class="middle-data">
                            <div class="middle-divide">
                                <div class="unit-data">
                                    <p>Name</p>
                                    <div class="kotak-middledata">
                                    <input type="text" name="nama"value="{{$email->nama}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Address</p>
                                    <div class="kotak-middledata">
                                    <input type="text" name="address" value="{{$email->address}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Email</p>
                                    <div class="kotak-middledata">
                                    <input type="email" name="email" value="{{$email->email}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Birth Place</p>
                                    <div class="kotak-middledata">
                                            <input type="text" name="provience" value="{{$email->provience}}">
                                    </div>
                                </div>
                            </div>
                            <div class="middle-divide">
                                <div class="unit-data">
                                    <p>WhatsApp Number</p>
                                    <div class="kotak-middledata">
                                            <input type="tel" name="phone" value="{{$email->phone}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Line ID</p>
                                    <div class="kotak-middledata">
                                            <input type="text" name="line" value="{{$email->line}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Gitlab ID</p>
                                    <div class="kotak-middledata">
                                            <input type="text" name="git" value="{{$email->git}}">
                                    </div>
                                </div>
                                <div class="unit-data">
                                    <p>Birth Date</p>
                                    <div class="kotak-middledata">
                                            <input type="date" name="date" value="{{$email->date}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="garis"></div>
                        <div class="middle-upload">
                            <div class="bawah-garis">

                                <div class="upload-btn-wrapper">
                                <input type="file" name="cv" id="file-upload">
                                <button class="btn" id="file-upload-filename" onclick="fontFunction()"><p id="file">{{$email->uploaduser->cv ?? 'CV Not Found'}}</p></button>

                                </div>
                                <i class="fas fa-folder logo-folder"></i>
                                <h3>|</h3>
                        </div>
                    <a href="{{ Storage::disk('local')->url($email->uploaduser->cv ?? 'CV Not Found')}}" target="_blank">
                        <button class="viewcv" type="submit" id="myBtn">View CV</button>
                        </a>
                        <div id="myModal" class="modal">

                              <!-- Modal content -->
                              <div class="modal-content">
                                <span class="close">&times;</span>
                                <p>Some text in the Modal..</p>
                                <img src="">
                              </div>

                        </div>
                    </div>
                    </div>
                </div>

            </div>
            </div>
        </form>
            @endforeach
            <div class="save-data">
                <button type="submit">Save Data</button>
                <button><a href="{{url('/adminEP')}}">Back</a></button>
            </div>
    </div>


        <script type="text/javascript">

            var input = document.getElementById( 'file-upload' );
            var infoArea = document.getElementById( 'file-upload-filename' );

            input.addEventListener( 'change', showFileName );

            function showFileName( event ) {

              // the change event gives us the input it occurred in
              var input = event.srcElement;

              // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
              var fileName = input.files[0].name;
              document.getElementById("file-upload-filename").style.fontFamily = "Montserrat, sans-serif";
              // use fileName however fits your app best, i.e. add it into a div
              infoArea.textContent = fileName;
            }


            /*Modal*/
            // Get the modal
            var modal = document.getElementById('myModal');
            var modal1 = document.getElementById('myModal1');
            var modal2 = document.getElementById('myModal2');

            // Get the button that opens the modal
            var btn = document.getElementById("myBtn");
            var btn1 = document.getElementById("myBtn1");
            var btn2 = document.getElementById("myBtn2");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];
            var span1 = document.getElementsByClassName("close1")[0];
            var span2 = document.getElementsByClassName("close2")[0];

            // When the email clicks on the button, open the modal
            btn.onclick = function() {
              modal.style.display = "block";
            }
            btn1.onclick = function(){
                modal1.style.display = "block";
            }
            btn2.onclick = function(){
                modal2.style.display = "block";
            }

            // When the email clicks on <span> (x), close the modal
            span.onclick = function() {
              modal.style.display = "none";
            }
            span1.onclick = function(){
                modal1.style.display = "none";
            }
            span2.onclick = function(){
                modal2.style.display = "none";
            }

            // When the email clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
              if (event.target == modal) {
                modal.style.display = "none";


              }else if(event.target == modal1){
                   modal1.style.display = "none";
              }else if(event.target == modal2){
                  modal2.style.display = "none";
              }
            }
            </script>

</div>

@endsection
