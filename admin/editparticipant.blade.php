@extends('layouts.adminbar')
{{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
<link rel="stylesheet" type="text/css" href="../../css/admin1.css">

@section('admin')
<div class="content">
        <div class="inside-content">
            <div class="title-content">
                <h1>Edit Participant Data</h1>
            </div>
            <div class="body-content">
                <!-- <div class="data-baris">

                </div> -->
                @foreach($users as $user)
                    @if($user->id != 1)
                    <div class="data-uni">
                        <div class="data">
                            <h1>{{$user->group}}</h1>
                            <div class="edit-data">
                                    <a type="btn btn-alert" href="{{action('UserController@store', $user['id'])}}">Edit Data</a>
                                </div>
                        </div>

                    </div>
                    @endif
                @endforeach
            </div>
        </div>

    </div>
@endsection
