<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>
	@extends('layouts.adminbar')
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/admin3.css">
	<link rel="stylesheet" href="css/all.css">
</head>
@section('admin')
<body>
	<div class="bodi">
		<div class="navbar">
			<div class="sidebar">
				<div class="container">
					<div class="container-kecil">
						<div class="logo">
							<div class="title">
								<h1>BTE</h1>
								<p>Admin Panel</p>
							</div>
						</div>
					
					<div class="home1">
						<div class="home active" id="js-menu">
			
						<div class="nav" id="js-menu">
							<div class="navnav">
								<div class="nav-choice ">
								<a href="admin1.html"><img src="assets/AdminPanel/Icon/PaymentConfirmBtn.png">
								</a>
								</div>

								<div class="nav-choice nav-active">
								<a href="admin2.html"><img src="assets/AdminPanel/Icon/EditDataBtn.png">
								</a>
								</div>
								<div class="nav-choice ex">
								<a href="#Logout"><img src="assets/AdminPanel/Icon/AdminLogoutBtn.png">
								</a>
								</div>
							</div>
						</div>
						
						
						</div>
						<div class="blur active1" id="js-menu1">
							
						</div>
					</div>
					
					</div>
					
				</div>
			</div>

			
		</div>
		
		<div class="content">
			<div class="inside-content">
				<div class="title-content">
					<h1>{{$users->find($id)->group}}</h1>
				</div>
				@foreach($uploads as $email)
				<form method="POST" action="/admin/{id}/edit" enctype="multipart/form-data">
				<div class="body-content">
					
					<div class="middle-content">
					<div class="middle-bio">
						<div class="md-content">
							<h1>{{$email->role}}</h1>
							<div class="middle-data">
								<div class="middle-divide">
									<div class="unit-data">
										<p>Name</p>
										<div class="kotak-middledata">
										<input type="text" name="nama"value="{{$email->nama}}">
										</div>
									</div>
									<div class="unit-data">
										<p>Address</p>
										<div class="kotak-middledata">
											<input type="text" name="address" value="{{$email->address}}">
										</div>
									</div>
									<div class="unit-data">
										<p>Email</p>
										<div class="kotak-middledata">
											<input type="email" name="email" value="{{$email->email}}">
										</div>
									</div>
									<div class="unit-data">
										<p>Birth Place</p>
										<div class="kotak-middledata">
											<input type="text" name="provience" value="{{$email->provience}}">
										</div>
									</div>
								</div>
								<div class="middle-divide">
									<div class="unit-data">
										<p>WhatsApp Number</p>
										<div class="kotak-middledata">
										<input type="tel" name="phone" value="{{$email->phone}}">
										</div>
									</div>
									<div class="unit-data">
										<p>Line ID</p>
										<div class="kotak-middledata">
										<input type="text" name="line" value="{{$email->line}}">
										</div>
									</div>
									<div class="unit-data">
										<p>Gitlab ID</p>
										<div class="kotak-middledata">
										<input type="text" name="git" value="{{$email->git}}">
										</div>
									</div>
									<div class="unit-data">
										<p>Birth Date</p>
										<div class="kotak-middledata">
										<input type="date" name="date" value="{{$email->date}}">
										</div>
									</div>
								</div>
							</div>
							<div class="garis"></div>
							<div class="middle-upload">
								<div class="bawah-garis"> 

									<div class="upload-btn-wrapper">
									<input type="file" name="cv" id="file-upload">
									<button class="btn" id="file-upload-filename" onclick="fontFunction()"><p id="file">{{$email->uploaduser->cv ?? 'CV Not Found'}}</p></button>
									
									</div>
									<i class="fas fa-folder logo-folder"></i>
									<h3>|</h3>
									
              						
								   
							</div>
							<a href="{{ Storage::disk('local')->url($email->uploaduser->cv ?? 'CV Not Found')}}" target="_blank"></a>
							<button class="viewcv" type="submit" id="myBtn">View CV</button>
							<div id="myModal" class="modal">

  								<!-- Modal content -->
  								<div class="modal-content">
  								  <span class="close">&times;</span>
  							 	 <p>Some text in the Modal..</p>
  							 	 <img src="">
  								</div>

							</div>
						</div>
						</div>	
					</div>	
					
				</div>
				</div>
				</form>
				@endforeach
				<div class="save-data">
					<button type="submit">Save Data</button>
					<button><a href="{{url('/adminEP')}}">Back</a></button>
				</div>
			</div>
			
		</div>
	</div>
	
<script type="text/javascript" src="js/admin3.js"></script>



	
</body>
</html>